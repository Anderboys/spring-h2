package ander.com.h2prueba;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class H2pruebaApplication {

    public static void main(String[] args) {
        SpringApplication.run(H2pruebaApplication.class, args);
    }



}
